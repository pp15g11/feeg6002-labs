#include <limits.h>
#include <stdio.h>
#include <math.h>

long maxlong(void) {
	
	return LONG_MAX;
}

double upper_bound(long n) {
	double result=1;
	int i;
	if (n>=0 && n<6) {
		if (n==0) {
			result=1;
		}
		else {
			for (i=1;i<=n;i++) {
				result=result*i;
			}
		}
	}
	else {
		result = pow((n/2.0),n);
	}
	
	return result;
}

long factorial(long n) {
	long i;
	long result=1;
	if (n<0) {
		result=-2;
	}
	else {
		if (upper_bound(n)>maxlong()) {
			result=-1;
		}
		else {
			for (i=1; i<=n;i++) {
				result=result*i;
				/*printf("%ld, %ld\n", i, result);*/
			}
		}
	}

	return result;
}

int main(void) {
    long i;	   

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */
	
    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
		/*printf("factorial(%ld)=%ld\n", i, factorial(i));*/
    }
    
    return 0;
}

