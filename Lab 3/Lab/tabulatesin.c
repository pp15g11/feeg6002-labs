#include <stdio.h>
#include <math.h>

#define N 10			/* total number of lines */
#define XMIN 1.0	   /* starting with x=a*/
#define XMAX 10.0	   /* ending with x=b*/

int main(void) {
	double x,y;
	int i;

	for (i=0;i<N;++i) {
		x=XMIN+(XMAX-XMIN)/(N-1)*i;
		y=sin(x);
		printf("%f %f\n",x,y);
	}
	return 0;
}


