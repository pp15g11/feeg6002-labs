/* Laboratory 6, SESG6025, 2013/2014, Template */

#include <stdio.h>
#define MAXLINE 1000 /* maximum length of string */

/* Function void rstrip(char s[])
modifies the string s: if at the beginning of the string s there are one or more spaces,
then remove these from the string.

The name rstrip stands for Right STRIP, trying to indicate that spaces at the 'right'
end of the string should be removed.
*/

long string_length(char s[]) {
	long c=0;
	while (s[c]!='\0') {
		c++;
	}
	return c;
}

void lstrip(char s[]) {
    /* to be implemented */
	long i=0;
	long n=0;
	long j=0;
	
	char new[MAXLINE];
	
	i=string_length(s);
	/* printf("%ld",i); */
	
	if ((i==1) && (s[0]=32)) {
		s[0]='\0';
	}
	else {
		for (n=0;n<=i-1;n++) {
			/*printf("%c\n",s[i-1-n]);  */
			if (s[n]!=32) {
				break;
			}
		}
		/* printf("%c\n",s[n]); */
		for (j=0;j<=i-n;j++) {
			new[j]=s[n+j];
		}
		for (j=0;j<=i-n;j++) {
			s[j]=new[j];
		}
		
		/*s[i-n]='\0';*/
	}
	
}


int main(void) {
  char test1[] = "  Hello World";
  /*char test1[] = " ";*/

  printf("Original string reads  : |%s|\n", test1);
  lstrip(test1);
  printf("l-stripped string reads: |%s|\n", test1);

  return 0;
}

