/* Laboratory 6, SESG6025, 2013/2014, Template */

#include <stdio.h>

/* Function void rstrip(char s[])
modifies the string s: if at the end of the string s there are one or more spaces,
then remove these from the string.

The name rstrip stands for Right STRIP, trying to indicate that spaces at the 'right'
end of the string should be removed.
*/

long string_length(char s[]) {
	long c=0;
	while (s[c]!='\0') {
		c++;
	}
	return c;
}

void rstrip(char s[]) {
    /* to be implemented */
	long i=0;
	long n=0;
	
	i=string_length(s);
	/* printf("%ld",i); */
	
	if ((i==1) && (s[0]=32)) {
		s[0]='\0';
	}
	else {
		for (n=0;n<=i-1;n++) {
			/*printf("%c\n",s[i-1-n]);  */
			if (s[i-1-n]!=32) {
				break;
			}
		}
		s[i-n]='\0';
	}
	
}


int main(void) {
  char test1[] = "Hello World   ";
  /*char test1[] = " ";*/

  printf("Original string reads  : |%s|\n", test1);
  rstrip(test1);
  printf("r-stripped string reads: |%s|\n", test1);

  return 0;
}

