#include<stdio.h>
/* TIMING CODE BEGIN (We need the following lines to take the timings.) */
#include<stdlib.h>
#include<math.h>
#include <time.h>
clock_t startm, stopm;
#define RUNS 1
#define START if ( (startm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define STOP if ( (stopm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define PRINTTIME printf( "%8.5f seconds used \n", (((double) stopm-startm)/CLOCKS_PER_SEC/RUNS));
/* TIMING CODE END */

double f(double x) {
	double result=0;
	result=sqrt(1.0-(x*x));
	return result;
	
}

double pi(long n) {
	double a,b,h,x,s,pi,i;
	a=-1.0;
	b=1.0;
	h=(b-a)/n;
	/* printf("%f\n",h);*/
	s=(0.5*f(a)) + (0.5*f(b));
	/*printf("%f\n",s);*/
	for (i=1;i<=n-1;++i) {
		x=a+(i*h);
		s=s+f(x);
	}
	pi=s*h*2;
	return pi;
}

int main(void) {
    /* Declarations */
	long n;
	double ans;


    /* Code */
    START;               /* Timing measurement starts here */
    /* Code to be written by student, calling functions from here is fine
       if desired
    */
	
	n=10000000;
	ans=pi(n);
	printf("%f\n", ans);
	

    STOP;                /* Timing measurement stops here */
    PRINTTIME;           /* Print timing results */
    return 0;
}

