#include <stdio.h>
#define MAXLINE 1000 /* maximum length of string */

/* function prototype */

long string_length(char s[]) {
	long c=0;
	while (s[c]!='\0') {
		c++;
	}
	return c;
}

void reverse(char source[], char target[]) {
	long i=0;	/* logging the number of characters */
	long n=0;
	
	i=string_length(source);
	/*printf("i=%ld\n",i);*/
	/*printf("last character: %s",source[i-1]);*/
	for (n=0;n<=(i-1);n++) {
		target[n]=source[i-1-n];
	}
	target[n+1]='\0';
	/*printf("%s\n",target);*/
	
}

int main(void) {
  char original[] = "This is a test: can you print me in reverse character order?";
  char reversed[MAXLINE];

  printf("%s\n", original);
  reverse(original, reversed);
  printf("%s\n", reversed);
  getchar();
  return 0;
}

/* reverse the order of characters in 'source', write to 'target'.
   Assume 'target' is big enough. 
void reverse(char source[], char target[]) {
}*/

