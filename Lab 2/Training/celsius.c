#include<stdio.h>

int main(void) {
	
	double T_Fahrenheit=0;
	double T_Celsius=0;
	
	for (T_Celsius=-30;T_Celsius<=30;T_Celsius=T_Celsius+2) {
		T_Fahrenheit = (T_Celsius*9/5)+32;
		printf("%3.0f = %5.1f\n",T_Celsius,T_Fahrenheit);
	}
	
	return 0;
}
		

