#include<stdio.h>

int main(void) {
	int s = 1000;		/* Sum borrowed in GBP */
	double debt = s;	/* Debt */
	double rate=0.03;	/* Interest rate */
	double interest;
	double i;
	double total_interest=0;
	double frac=0;
	for (i=1;i<=24;++i) {
		interest = debt * rate;
		debt = debt + interest;
		total_interest = total_interest+interest;
		frac=(total_interest/s)*100;
		printf("month %2.0f: debt=%4.2f, interest=%2.2f, total_interest=%7.2f, frac=%6.2f%%\n",i,debt,interest,total_interest,frac);
	}
	return 0;
}

